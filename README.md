# Project Euler - Java

My attempt at tackling Project Euler using (and learning) Java, around 2009.

# Overview

I'm surprised there's no `build.xml` or `pom.xml` file - this was around the time that I was really big on Apache Ant (I'd just had a gig developing a Swing app that used Ant to migrate a Powerbuilder app from Visual SourceSafe to Subversion) and was also looking at Maven as an alternative. It should be pretty straightforward to build, though.

## Useful links

### Project

* Project Euler: https://projecteuler.net/ (I've long since lost *that* login!)
* git.coop: https://git.coop/johnn/project-euler-java
* GitLab Markdown cheatsheet: https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet

# License

Copyright (c) 2009-2021 John Niven

Licensed under the GPL, v3.
