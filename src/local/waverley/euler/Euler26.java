/**
 * 
 */
package local.waverley.euler;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler26
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler26.class);
	
	private static final int TARGET = 10;
	
	/**
	 * A unit fraction contains 1 in the numerator. The decimal representation
	 * of the unit fractions with denominators 2 to 10 are given:
	 * 
	 * 		1/2	= 	0.5
	 * 		1/3	= 	0.(3)
	 * 		1/4	= 	0.25
	 * 		1/5	= 	0.2
	 * 		1/6	= 	0.1(6)
	 * 		1/7	= 	0.(142857)
	 * 		1/8	= 	0.125
	 * 		1/9	= 	0.(1)
	 * 	   1/10	= 	0.1
	 * Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can
	 * be seen that 1/7 has a 6-digit recurring cycle.
	 * 
	 * Find the value of d  1000 for which 1/d contains the longest recurring
	 * cycle in its decimal fraction part.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Set<BigDecimal> decimals = new TreeSet<BigDecimal>();
		
		for (int i = 2; i <= TARGET; i++)
		{
			BigDecimal decimal = (new BigDecimal(1)).divide(new BigDecimal(i, new MathContext(100, RoundingMode.HALF_UP)));
			decimals.add(decimal);
			LOGGER.debug("1/{} = {}", i, decimal.toString());
		}
		
		for (BigDecimal decimal : decimals)
		{
			LOGGER.debug("{}", decimal.toString());
		}
	}

}
