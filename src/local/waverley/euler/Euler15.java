/**
 * 
 */
package local.waverley.euler;

import java.util.Iterator;
import java.util.List;

import local.waverley.maths.Utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler15
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler15.class);
	
	private static final int GRID_SIZE = 20;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		Iterator<List<Long>> triangle = Utility.getPascalsTriangle();
		
		List<Long> row = null;
		int i = 0;
		while (triangle.hasNext())
		{
			row = triangle.next();
			if ((i % 2) == 0)
			{
				int rowNum = i /2;
				if (rowNum == GRID_SIZE)
				{
					break;
				}
			}
			i++;
		}
		
		LOGGER.info("Number of routes: {}", row.get(row.size() / 2));
	}
}
