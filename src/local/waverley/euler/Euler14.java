/**
 * 
 */
package local.waverley.euler;

import java.util.Iterator;

import local.waverley.maths.Utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler14
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler14.class);

	private static final int MAX = 1000000;
	
	/**
	 * The following iterative sequence is defined for the set of positive integers:
	 * 
	 * 		n -> n/2 (n is even)
	 * 		n -> 3n + 1 (n is odd)
	 * 
	 * Using the rule above and starting with 13, we generate the following
	 * sequence:
	 * 
	 * 		13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
	 * 
	 * It can be seen that this sequence (starting at 13 and finishing at 1)
	 * contains 10 terms. Although it has not been proved yet (Collatz Problem),
	 * it is thought that all starting numbers finish at 1.
	 * 
	 * Which starting number, under one million, produces the longest chain?
	 * 
	 * NOTE: Once the chain starts the terms are allowed to go above one
	 * million.

	 * @param args
	 */
	public static void main(String[] args)
	{
		int startSeed = 0;
		int greatestLength = 0;
		
		for (int i = 1; i < MAX; i++)
		{
			Iterator<Long> hailstones = Utility.getHailstoneSequence(i);
			int chainLength = 0;
			while (hailstones.hasNext())
			{
				chainLength++;
				hailstones.next();
			}
			
			if (chainLength > greatestLength)
			{
				startSeed = i;
				greatestLength = chainLength;
			}
		}
		
		LOGGER.info("RESULT: Chain starting with {} yielded a chain-length of {}", startSeed, greatestLength);
	}

}
