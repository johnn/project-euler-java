/**
 * 
 */
package local.waverley.euler;

import java.util.Set;
import java.util.TreeSet;

import local.waverley.maths.Perfect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler23
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler23.class);
	
	private static final int TARGET = 28123;

	private static Set<Long> abundantNumbers = new TreeSet<Long>();
	
	/**
	 * A perfect number is a number for which the sum of its proper divisors is
	 * exactly equal to the number. For example, the sum of the proper divisors
	 * of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect
	 * number.
	 * 
	 * A number n is called deficient if the sum of its proper divisors is less
	 * than n and it is called abundant if this sum exceeds n.
	 * 
	 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the
	 * smallest number that can be written as the sum of two abundant numbers is
	 * 24. By mathematical analysis, it can be shown that all integers greater
	 * than 28123 can be written as the sum of two abundant numbers. However,
	 * this upper limit cannot be reduced any further by analysis even though it
	 * is known that the greatest number that cannot be expressed as the sum of
	 * two abundant numbers is less than this limit.
	 * 
	 * Find the sum of all the positive integers which cannot be written as the
	 * sum of two abundant numbers.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Set up abundant numbers
		for (long i = 1; i <= TARGET; i++)
		{
			if (Perfect.getPerfection(i) == Perfect.ABUNDANT)
			{
				abundantNumbers.add(i);
			}
		}

		// Calculate sum of numbers that can be written as the sum of two abundant numbers
		long sum = 0;
		for (long i = 1; i <= TARGET; i++)
		{
			if(!canBeWrittenAsSumOfTwoAbundantNumbers(i))
			{
				sum += i;
			}
		}
		
		LOGGER.info("Result: {}", sum);
	}
	
	private static boolean canBeWrittenAsSumOfTwoAbundantNumbers(long number)
	{
		boolean retVal = false;
		
		for (long abundantNumber : abundantNumbers)
		{
			if (abundantNumbers.contains(number - abundantNumber))
			{
				retVal = true;
				break;
			}
		}
		
		return retVal;
	}

}
