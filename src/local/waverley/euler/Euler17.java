/**
 * 
 */
package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler17
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler17.class);
	
	private static final int TARGET = 1000;
	
	private static final String[] simpleWords = {
			"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
			"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen",
			"Twenty"			
	};
	private static final String[] decaWords = {
			"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"
	};
	
	/**
	 * If the numbers 1 to 5 are written out in words: one, two, three, four,
	 * five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
	 * 
	 * If all the numbers from 1 to 1000 (one thousand) inclusive were written
	 * out in words, how many letters would be used?
	 * 
	 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
	 * forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
	 * 20 letters. The use of "and" when writing out numbers is in compliance
	 * with British usage.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		int letterCount = 0;
		for (int i = 1; i <= TARGET; i++)
		{
			letterCount += getLetterCount(i);
		}

		LOGGER.info("Result: {}", letterCount);
	}
	
	private static int getLetterCount(int number)
	{
		StringBuffer sb = new StringBuffer();
		
		if (number < 21)
		{
			sb.append(getSimple(number));
		}
		else if (number >= 21 && number < 100)
		{
			sb.append(getDeca(number));
		}
		else if (number >= 100 && number < 1000)
		{
			sb.append(getCenta(number));
		}
		else if (number == 1000)
		{
			sb.append("OneThousand");
		}
		else
		{
			sb.append(">>> Help murder polis! <<<");
		}
		
		return sb.length();
	}
	
	private static String getSimple(int number)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(simpleWords[number]);
		return sb.toString();
	}

	private static String getDeca(int number)
	{
		StringBuffer sb = new StringBuffer();
		int deca = number / 10;
		int simple = number % 10;
		sb.append(decaWords[deca]).append(getSimple(simple));
		return sb.toString();
	}
	
	private static String getCenta(int number)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(getSimple(number / 100)).append("Hundred");
		int remainder = number % 100;
		if (!(remainder == 0))
		{
			sb.append("And");
			if (remainder < 21)
			{
				sb.append(getSimple(remainder));
			}
			else
			{
				sb.append(getDeca(remainder));
			}
		}
		return sb.toString();
	}
}
