package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Euler1
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler1.class);
	
	/**
	 * If we list all the natural numbers below 10 that are multiples of 3 or 5,
	 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
	 * Find the sum of all the multiples of 3 or 5 below 1000.
	 * @param args
	 */
	public static void main(String[] args)
	{
		int sum = 0;
		
		for (int i = 1; i < 1000; i++)
		{
			if (isMultipleOfThreeOrFive(i))
			{
				sum += i;
			}
		}
		
		LOGGER.info("Sum is... {}", sum);
	}

	private static boolean isMultipleOfThreeOrFive(int value)
	{
		if ((value % 3) == 0)
		{
			return true;
		}
		else if ((value % 5) == 0)
		{
			return true;
		}
		return false;
	}
}
