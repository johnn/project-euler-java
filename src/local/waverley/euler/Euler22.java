/**
 * 
 */
package local.waverley.euler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler22
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler22.class);
	
	/**
	 * Using names.txt (right click and 'Save Link/Target As...'), a 46K text
	 * file containing over five-thousand first names, begin by sorting it into
	 * alphabetical order. Then working out the alphabetical value for each
	 * name, multiply this value by its alphabetical position in the list to
	 * obtain a name score.
	 * 
	 * For example, when the list is sorted into alphabetical order, COLIN,
	 * which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
	 * So, COLIN would obtain a score of 938  53 = 49714.
	 * 
	 * What is the total of all the name scores in the file?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		List<String> names = getNames("/home/john/Downloads/names.txt");
		Object[] array = names.toArray();
		Arrays.sort(array);

		int totalScore = 0;
		
		for (int i = 0; i < array.length; i++)
		{
			int score = (i + 1) * alphabeticalValue((String) array[i]);
			totalScore += score;
		}
		LOGGER.info("Result: {}", totalScore);
	}
	
	private static int alphabeticalValue(String text)
	{
		int retVal = 0;
		
		for (int i = 0; i < text.length(); i++)
		{
			Character letter = text.charAt(i);
			int letterValue = Character.getNumericValue(letter.charValue()) - 9;
			retVal += letterValue;
		}
		
		return retVal;
	}

	private static List<String> getNames(String fileName)
	{
		List<String> names = new ArrayList<String>();
		FileReader fr;
		try
		{
			fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);

			String s;
			while ((s = br.readLine()) != null)
			{
				StringTokenizer st = new StringTokenizer(s, ",");
				while (st.hasMoreTokens())
				{
					String rawName = st.nextToken();
					String name = rawName.substring(1, rawName.length() - 1);
					names.add(name.toUpperCase());
				}
			}
		}
		catch (FileNotFoundException e)
		{
			LOGGER.error(e.getLocalizedMessage());
		}
		catch (IOException e)
		{
			LOGGER.error(e.getLocalizedMessage());
		}
		
		return names;
	}
}
