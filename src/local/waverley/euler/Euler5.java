/**
 * 
 */
package local.waverley.euler;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler5
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler5.class);
	
	private static final int MIN = 1;
	private static final int MAX = 20;
	
	private static int longestSB = 0;
	
	/**
	 * 2520 is the smallest number that can be divided by each of the numbers
	 * from 1 to 10 without any remainder.
	 * What is the smallest positive number that is evenly divisible by all of
	 * the numbers from 1 to 20? 
	 * @param args
	 */
	public static void main(String[] args)
	{
		List<Integer> divisors1 = new ArrayList<Integer>();
		// We can ... 
		//		ignore 1 (all numbers are cleanly divisible by 1).
		// 		ignore 2 (we only look at even numbers: no odd number is cleanly divisible by 2).
		//		ignore 20 (i increments by 20)
		// So...
		//		We loop from MIN + 2, to ignore 1 and 2...
		//		We loop to i < MAX to ignore 20.
		for (int i = MIN + 2; i < MAX; i++)
		{
			divisors1.add(i);
		}
		
		int i = 20;
		while (!isEvenlyDivisible(i, divisors1.listIterator(divisors1.size())))
		{
			i = i + 20;	// We know that i has to be cleanly divisible by 20.
		}
		LOGGER.info("Smallest number that is evenly divisible by {}..{} is {}", new Object[]{MIN, MAX, i});		
	}

	private static boolean isEvenlyDivisible(int number, ListIterator<Integer> divisors)
	{
		StringBuffer sb = new StringBuffer(String.valueOf(number));
		// Iterate backwards, to "fail fast".
		while (divisors.hasPrevious())
		{			
			int divisor = divisors.previous();
			int remainder = number % divisor;
			sb.append(", " + String.valueOf(divisor) + ": " + String.valueOf(remainder));
			if (remainder != 0)
			{
				if (sb.length() > longestSB)
				{
					longestSB = sb.length();
					LOGGER.info("Not evenly divisible by {}..{}: {}", new Object[]{MIN, MAX, sb.toString()});
				}
				return false;
			}		
		}
		LOGGER.info("Evenly divisible by {}..{}: {} !!!", new Object[]{MIN, MAX, sb.toString()});
		return true;
	}
}
