/**
 * 
 */
package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler9
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler9.class);
	
	private static final int TARGET = 1000;

	/**
	 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
	 * 		(a*a) + (b*b) = (c*c)
	 * 
	 * For example, (3*3) + (4*4) = 9 + 16 = 25 = (5*5).
	 * 
	 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
	 * Find the product abc.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		int a = 0, b = 0, c = 0;
		
		Outer:
		for (a = 1; a <= TARGET; a++)	// Scope for optimisation around TARGET...
		{
			for (b = a + 1; b <= TARGET; b++)
			{
				for (c = b + 1; c <= TARGET; c++)
				{
					if ((a + b + c) == TARGET)
					{
						LOGGER.info("a + b + c = {}", TARGET);
						if (((a * a) + (b * b)) == (c * c))
						{
							LOGGER.info("Solution: a: {}, b: {}, c: {}, abc: {}", new Object[]{a , b, c, (a * b * c)});
							break Outer;
						}
					}
				}
			}
		}
	}
}
