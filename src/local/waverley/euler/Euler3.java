package local.waverley.euler;

import java.util.Iterator;

import local.waverley.maths.prime.Factoriser;
import local.waverley.maths.prime.FactoriserProvider;
import local.waverley.maths.prime.SieveProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler3
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler3.class);
	
	//private static final int MAX_NUM = 50;
	
	/**
	 * The prime factors of 13195 are 5, 7, 13 and 29.
	 * What is the largest prime factor of the number 600851475143 ?
	 * @param args
	 */
	public static void main(String[] args)
	{
		/*
		Sieve sieve = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_TRIAL_DIVISION);
		LOGGER.info("Prime sieve (using trial division) is        {}", sieve.toString());

		Sieve sieve2 = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_OF_ERATOSTHENES);
		LOGGER.info("Prime sieve (using Sieve of Eratosthenes) is {}", sieve2);
		
		Sieve sieve3 = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_OF_SUNDARAM);
		LOGGER.info("Prime sieve (using Sieve of Sundaram) is     {}", sieve3);
		
		Factoriser factoriser = FactoriserProvider.getFactoriserByTrialDivision();
		Iterator<Long> iter = factoriser.getFactors(13195, SieveProvider.SIEVE_OF_ERATOSTHENES);
		while (iter.hasNext())
		{
			long factor = iter.next();
			LOGGER.info("Factor: {}", factor);
		}
		*/
		
		Factoriser factoriser = FactoriserProvider.getFactoriserByTrialDivision();
		long number = Long.parseLong("600851475143");
		Iterator<Long> iter = factoriser.getFactors(number, SieveProvider.SIEVE_OF_ERATOSTHENES);
		while (iter.hasNext())
		{
			long factor = iter.next();
			LOGGER.info("Factor: {}", factor);
		}
	}
}
