/**
 * 
 */
package local.waverley.euler;

import java.util.ArrayList;
import java.util.List;

import local.waverley.utility.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler18
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler18.class);

	private static final int[][] values = {
			{ 75 },
			{ 95, 64 },
			{ 17, 47, 82 },
			{ 18, 35, 87, 10 },
			{ 20,  4, 82, 47, 65 },
			{ 19,  1, 23, 75,  3, 34 },
			{ 88,  2, 77, 73,  7, 63, 67 },
			{ 99, 65,  4, 28,  6, 16, 70, 92 },
			{ 41, 41, 26, 56, 83, 40, 80, 70, 33 },
			{ 41, 48, 72, 33, 47, 32, 37, 16, 94, 29 },
			{ 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14 },
			{ 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57 },
			{ 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48 },
			{ 63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31 },
			{  4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23 }
	};
	
	private static final Euler18 instance = new Euler18();
	
	private static final Triangle NULL_TRIANGLE = instance.new NullTriangle();
	
	/**
	 * By starting at the top of the triangle below and moving to adjacent
	 * numbers on the row below, the maximum total from top to bottom is 23.
	 * 
	 *      >3<
	 *    >7<  4
	 *   2  >4<  6
	 * 8   5  >9<  3
	 * 
	 * That is, 3 + 7 + 4 + 9 = 23.
	 * 
	 * Find the maximum total from top to bottom of the triangle below:
	 * 
	 * 75
	 * 95 64
	 * 17 47 82
	 * 18 35 87 10
	 * 20 04 82 47 65
	 * 19 01 23 75 03 34
	 * 88 02 77 73 07 63 67
	 * 99 65 04 28 06 16 70 92
	 * 41 41 26 56 83 40 80 70 33
	 * 41 48 72 33 47 32 37 16 94 29
	 * 53 71 44 65 25 43 91 52 97 51 14
	 * 70 11 33 28 77 73 17 78 39 68 17 57
	 * 91 71 52 38 17 14 91 43 58 50 27 29 48
	 * 63 66 04 68 89 53 67 30 73 16 69 87 40 31
	 * 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
	 * 
	 * NOTE: As there are only 16384 routes, it is possible to solve this
	 * problem by trying every route. However, Problem 67, is the same challenge
	 * with a triangle containing one-hundred rows; it cannot be solved by brute
	 * force, and requires a clever method! ;o)
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Triangle topTriangle = instance.new TriangleImpl(values[0][0], NULL_TRIANGLE, NULL_TRIANGLE);
		
		List<List<Triangle>> triangles = initialiseTriangles(topTriangle);
		
		for (int rowNum = triangles.size() - 1; rowNum >= 0; rowNum--)
		{
			List<Triangle> row = triangles.get(rowNum);
			for (Triangle triangle : row)
			{
				triangle.resetValue();
			}
		}
		
		LOGGER.info("Result: {}", topTriangle.getValue());
	}
	
	private static List<List<Triangle>> initialiseTriangles(Triangle topTriangle)
	{
		List<List<Triangle>> triangles = new ArrayList<List<Triangle>>();
		
		List<Triangle> triangleRow = new ArrayList<Triangle>();		
		triangleRow.add(NULL_TRIANGLE);
		triangleRow.add(topTriangle);
		triangleRow.add(NULL_TRIANGLE);
		
		triangles.add(triangleRow);

		List<Triangle> lastRow = triangleRow;
		
		for (int row = 1; row < values.length; row++)
		{
			triangleRow = new ArrayList<Triangle>();
			triangleRow.add(NULL_TRIANGLE);
			
			int[] rowArray = values[row];
			for (int column = 0; column < rowArray.length; column++)
			{
				Triangle triangle = instance.new TriangleImpl(values[row][column], lastRow.get(column), lastRow.get(column + 1));
				triangleRow.add(triangle);				
			}
			
			triangleRow.add(NULL_TRIANGLE);
			triangles.add(triangleRow);
			lastRow = triangleRow;
		}
		
		return triangles;
	}
	
	interface Triangle extends Nullable
	{
		public void setLeftChild(Triangle child);
		public void setRightChild(Triangle child);
		public void resetValue();
		public int getValue();
		public Triangle getLeftParent();
		public Triangle getRightParent();
		public Triangle getLeftChild();
		public Triangle getRightChild();
	}
	
	class TriangleImpl implements Triangle
	{
		private int value;
		private Triangle leftChild = Euler18.NULL_TRIANGLE;
		private Triangle rightChild = Euler18.NULL_TRIANGLE;
		private final Triangle leftParent;
		private final Triangle rightParent;
		
		TriangleImpl(int value, Triangle leftParent, Triangle rightParent)
		{
			this.value = value;
			this.leftParent = leftParent;
			leftParent.setRightChild(this);
			this.rightParent = rightParent;
			rightParent.setLeftChild(this);
		}
		
		public boolean isNull() { return false; }
		
		public void setLeftChild(Triangle child)
		{
			if (!isNull())
			{
				this.leftChild = child;
			}
		}
		
		public void setRightChild(Triangle child)
		{
			if (!isNull())
			{
				this.rightChild = child;
			}
		}

		@Override
		public int getValue() { return this.value; }

		@Override
		public Triangle getLeftParent() { return this.leftChild; }

		@Override
		public Triangle getRightParent() { return this.rightChild; }

		@Override
		public Triangle getLeftChild() { return this.leftParent; }

		@Override
		public Triangle getRightChild() { return this.rightParent; }
		
		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("T v").append(this.value);
			sb.append(" lp").append(this.leftParent.getValue());
			sb.append(" rp").append(this.rightParent.getValue());
			sb.append(" lc").append(this.leftChild.getValue());
			sb.append(" rc").append(this.rightChild.getValue());
			
			return sb.toString();
		}

		@Override
		public void resetValue()
		{
			int left = this.leftChild.getValue();
			int right = this.rightChild.getValue();
			int value = Math.max(left, right);
			this.value += value;			
		}
	}
	
	class NullTriangle implements Triangle
	{
		public boolean isNull() { return true; }

		@Override
		public void setLeftChild(Triangle child) {}

		@Override
		public void setRightChild(Triangle child) {}
		
		@Override
		public Triangle getLeftParent() { return this; }

		@Override
		public Triangle getRightParent() { return this; }

		@Override
		public Triangle getLeftChild() { return this; }

		@Override
		public Triangle getRightChild() { return this; }

		@Override
		public int getValue() { return 0; }
		
		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("T v").append(0);
			sb.append(" lp").append("-");
			sb.append(" rp").append("-");
			sb.append(" lc").append("-");
			sb.append(" rc").append("-");
			
			return sb.toString();
		}

		@Override
		public void resetValue() {}
	}
}
