package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fibonacci
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Fibonacci.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		int term1 = 1;
		int term2 = 2;

		LOGGER.info("Term 1 : {}", term1);
		LOGGER.info("Term 2 : {}", term2);
		
		for (int i = 3; i <= 12; i++)
		{
			int termNext = term1 + term2;
			LOGGER.info("Term {} : {}", i, termNext);
			term1 = term2;
			term2 = termNext;
		}
		
		double number = Math.pow(2, 1000);
		LOGGER.info("2 raised to the 1000th power is: {}", number);
		
	}

}
