/**
 * 
 */
package local.waverley.euler;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler16
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler16.class);
	
	/**
	 * 2**15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
	 * 
	 * What is the sum of the digits of the number 2**1000?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		BigInteger two = new BigInteger("2");
		BigInteger number = two.pow(1000);
		
		String digits = number.toString();

		long sum = 0;
		
		for (int i = 0; i < digits.length(); i++)
		{
			String digit = digits.substring(i, i + 1);
			sum += Long.parseLong(digit);
		}
		
		LOGGER.info("Result: {}", sum);
	}

}
