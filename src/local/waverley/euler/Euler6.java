/**
 * 
 */
package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler6
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler6.class);
	
	private static final int MIN = 1;
	private static final int MAX = 100;
	
	/**
	 * The sum of the squares of the first ten natural numbers is,
	 * 		12 + 22 + ... + 102 = 385
	 * The square of the sum of the first ten natural numbers is,
	 * 		(1 + 2 + ... + 10)2 = 552 = 3025
	 * Hence the difference between the sum of the squares of the first ten
	 * natural numbers and the square of the sum is 3025  385 = 2640.
	 * 
	 * Find the difference between the sum of the squares of the first one
	 * hundred natural numbers and the square of the sum.
	 * @param args
	 */
	public static void main(String[] args)
	{
		int sumOfSquares = sumOfSquares(MIN, MAX);
		int squareOfSums = squareOfSums(MIN, MAX);

		LOGGER.info("Sum of squares: {}; square of sums: {}; Difference: {}", new Object[]{sumOfSquares, squareOfSums, Math.abs(sumOfSquares - squareOfSums)});
	}

	private static int sumOfSquares(int min, int max)
	{
		int retVal = 0;
		
		for (int i = min; i <= max; i++)
		{
			int square = i * i;
			retVal += square;
		}
		
		return retVal;
	}
	
	private static int squareOfSums(int min, int max)
	{
		int retVal = 0;
		
		for (int i = min; i <= max; i++)
		{
			retVal += i;
		}
		retVal *= retVal;
		
		return retVal;
	}
}
