/**
 * 
 */
package local.waverley.euler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import local.waverley.maths.Utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler24
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler24.class);

	private static final int[] digits = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	private static final int TARGET = 1000000;
	
	private static Map<Set<Integer>, Set<String>> map = new HashMap<Set<Integer>, Set<String>>();
	
	/**
	 * A permutation is an ordered arrangement of objects. For example, 3124 is
	 * one possible permutation of the digits 1, 2, 3 and 4. If all of the
	 * permutations are listed numerically or alphabetically, we call it
	 * lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
	 * 
	 * 		012   021   102   120   201   210
	 * 
	 * What is the millionth lexicographic permutation of the digits 0, 1, 2, 3,
	 * 4, 5, 6, 7, 8 and 9?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Initialise set of digits
		Set<Integer> setOfDigits = new TreeSet<Integer>();
		for (int i = 0; i < digits.length; i++)
		{
			setOfDigits.add(i);
		}

		Set<String> perms = null;
		perms = getPermutations(setOfDigits);
		
		int i = 1;
		String result = null;
		for (String perm : perms)
		{
			if (i == TARGET)
			{
				result = perm;
				break;
			}				
			i++;
		}
		
		LOGGER.info("Result: {}", result);
	}
	
	private static Set<String> getPermutations(Set<Integer> set)
	{
		Set<String> returnSet = new TreeSet<String>();
		// Don't waste time calculating permutations if they've already been
		// calculated - simply get them from the map.
		if (map.containsKey(set))
		{
			returnSet = map.get(set);
		}
		else
		{
			if (set.size() == 2)
			{
				Iterator<Integer> iter = set.iterator();
				String item1 = String.valueOf(iter.next());
				String item2 = String.valueOf(iter.next());
				String perm1 = item1 + item2;
				String perm2 = item2 + item1;
				returnSet.add(perm1);
				returnSet.add(perm2);
			}
			else if (set.size() >= 3)
			{
				for (Integer item : set)
				{
					Set<Integer> newSet = Utility.copy(set);
					newSet.remove(item);
					Set<String> newPerms = getPermutations(newSet);
					for (String perm : newPerms)
					{
						String newPerm = String.valueOf(item) + perm;
						returnSet.add(newPerm);
					}
					newSet = null;
				}
			}
			if (set.size() <= 6)	// Try and avoid the garbage collector overhead limit...
			{
				map.put(set, returnSet);
			}
		}
		return returnSet;
	}
}
