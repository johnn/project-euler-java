/**
 * 
 */
package local.waverley.euler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import local.waverley.utility.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler67
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler67.class);
	
	private static final Euler67 instance = new Euler67();
	
	private static final Triangle NULL_TRIANGLE = instance.new NullTriangle();
	
	/**
	 * By starting at the top of the triangle below and moving to adjacent
	 * numbers on the row below, the maximum total from top to bottom is 23.
	 * 
	 *      >3<
	 *    >7<  4
	 *   2  >4<  6
	 * 8   5  >9<  3
	 * 
	 * That is, 3 + 7 + 4 + 9 = 23.
	 * 
	 * Find the maximum total from top to bottom in triangle.txt (right click
	 * and 'Save Link/Target As...'), a 15K text file containing a triangle with
	 * one-hundred rows.
	 * 
	 * NOTE: This is a much more difficult version of Problem 18. It is not
	 * possible to try every route to solve this problem, as there are 2^99
	 * altogether! If you could check one trillion (10^12) routes every second it
	 * would take over twenty billion years to check them all. There is an
	 * efficient algorithm to solve it. ;o)
	 * 
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args)
	{
		int[][] values;
		try
		{
			values = getTriangles("/home/john/Downloads/triangle.txt", 100);
			
			Triangle topTriangle = instance.new TriangleImpl(values[0][0], NULL_TRIANGLE, NULL_TRIANGLE);
			
			List<List<Triangle>> triangles = initialiseTriangles(topTriangle, values);
			
			for (int rowNum = triangles.size() - 1; rowNum >= 0; rowNum--)
			{
				List<Triangle> row = triangles.get(rowNum);
				for (Triangle triangle : row)
				{
					triangle.resetValue();
				}
			}
			
			LOGGER.info("Result: {}", topTriangle.getValue());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static int[][] getTriangles(String fileName, int depth) throws Exception
	{
		int[][] values = new int[depth][];
		FileReader fr = new FileReader(fileName);
		BufferedReader br = new BufferedReader(fr);

		String s;
		int i = 0;
		while ((s = br.readLine()) != null)
		{
			values[i] = new int[i + 1];
			int j = 0;
			Scanner tokens = new Scanner(s);
			while (tokens.hasNext())
			{
				int value = tokens.nextInt();
				values[i][j++] = value;
			}
			i++;
		}

		return values;
	}
	
	private static List<List<Triangle>> initialiseTriangles(Triangle topTriangle, int[][] values)
	{
		List<List<Triangle>> triangles = new ArrayList<List<Triangle>>();
		
		List<Triangle> triangleRow = new ArrayList<Triangle>();		
		triangleRow.add(NULL_TRIANGLE);
		triangleRow.add(topTriangle);
		triangleRow.add(NULL_TRIANGLE);
		
		triangles.add(triangleRow);

		List<Triangle> lastRow = triangleRow;
		
		for (int row = 1; row < values.length; row++)
		{
			triangleRow = new ArrayList<Triangle>();
			triangleRow.add(NULL_TRIANGLE);
			
			int[] rowArray = values[row];
			for (int column = 0; column < rowArray.length; column++)
			{
				Triangle triangle = instance.new TriangleImpl(values[row][column], lastRow.get(column), lastRow.get(column + 1));
				triangleRow.add(triangle);				
			}
			
			triangleRow.add(NULL_TRIANGLE);
			triangles.add(triangleRow);
			lastRow = triangleRow;
		}
		
		return triangles;
	}
	
	interface Triangle extends Nullable
	{
		public void setLeftChild(Triangle child);
		public void setRightChild(Triangle child);
		public void resetValue();
		public int getValue();
		public Triangle getLeftParent();
		public Triangle getRightParent();
		public Triangle getLeftChild();
		public Triangle getRightChild();
	}
	
	class TriangleImpl implements Triangle
	{
		private int value;
		private Triangle leftChild = Euler67.NULL_TRIANGLE;
		private Triangle rightChild = Euler67.NULL_TRIANGLE;
		private final Triangle leftParent;
		private final Triangle rightParent;
		
		TriangleImpl(int value, Triangle leftParent, Triangle rightParent)
		{
			this.value = value;
			this.leftParent = leftParent;
			leftParent.setRightChild(this);
			this.rightParent = rightParent;
			rightParent.setLeftChild(this);
		}
		
		public boolean isNull() { return false; }
		
		public void setLeftChild(Triangle child)
		{
			if (!isNull())
			{
				this.leftChild = child;
			}
		}
		
		public void setRightChild(Triangle child)
		{
			if (!isNull())
			{
				this.rightChild = child;
			}
		}

		@Override
		public int getValue() { return this.value; }

		@Override
		public Triangle getLeftParent() { return this.leftChild; }

		@Override
		public Triangle getRightParent() { return this.rightChild; }

		@Override
		public Triangle getLeftChild() { return this.leftParent; }

		@Override
		public Triangle getRightChild() { return this.rightParent; }
		
		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("T v").append(this.value);
			sb.append(" lp").append(this.leftParent.getValue());
			sb.append(" rp").append(this.rightParent.getValue());
			sb.append(" lc").append(this.leftChild.getValue());
			sb.append(" rc").append(this.rightChild.getValue());
			
			return sb.toString();
		}

		@Override
		public void resetValue()
		{
			int left = this.leftChild.getValue();
			int right = this.rightChild.getValue();
			int value = Math.max(left, right);
			this.value += value;			
		}
	}
	
	class NullTriangle implements Triangle
	{
		public boolean isNull() { return true; }

		@Override
		public void setLeftChild(Triangle child) {}

		@Override
		public void setRightChild(Triangle child) {}
		
		@Override
		public Triangle getLeftParent() { return this; }

		@Override
		public Triangle getRightParent() { return this; }

		@Override
		public Triangle getLeftChild() { return this; }

		@Override
		public Triangle getRightChild() { return this; }

		@Override
		public int getValue() { return 0; }
		
		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("T v").append(0);
			sb.append(" lp").append("-");
			sb.append(" rp").append("-");
			sb.append(" lc").append("-");
			sb.append(" rc").append("-");
			
			return sb.toString();
		}

		@Override
		public void resetValue() {}
	}
}
