/**
 * 
 */
package local.waverley.euler;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import local.waverley.maths.prime.Sieve;
import local.waverley.maths.prime.SieveProvider;

/**
 * @author john
 *
 */
public class Euler7
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler7.class);

	private static final int MAX = 1000000;
	private static final int TARGET = 10001;
	
	/**
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can
	 * see that the 6th prime is 13.
	 * 
	 * What is the 10 001st prime number?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Sieve sieve = SieveProvider.getSieve(MAX, SieveProvider.SIEVE_OF_ERATOSTHENES);
		LOGGER.info("Prime sieve (using Sieve of Eratosthenes) is {}", sieve);

		Iterator<Long> primes = sieve.getPrimes();
		
		long i = 0;
		long prime = 0;
		while (primes.hasNext() && i <= TARGET)
		{
			prime = primes.next();
			LOGGER.info("Prime {} is {}", i, prime);
			i++;
		}
		LOGGER.info("The {}th prime number is {}", TARGET, prime);
	}

}
