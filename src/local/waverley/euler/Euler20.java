/**
 * 
 */
package local.waverley.euler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler20
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler20.class);
	
	private static final int TARGET = 100;
	
	/**
	 * n! means n * (n - 1)  ...  3 * 2 * 1
	 * 
	 * For example, 10! = 10 * 9  ...  3 * 2 * 1 = 3628800,
	 * and the sum of the digits in the number 10! is ... 
	 * 		3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
	 * 
	 * Find the sum of the digits in the number 100!
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		BigInteger one = BigInteger.ONE;
		List<BigInteger> factorials = new ArrayList<BigInteger>();
		factorials.add(one);
		
		for (int i = 1; i <= TARGET; i++)
		{
			BigInteger factorial = factorials.get(i - 1).multiply(new BigInteger(String.valueOf(i)));
			factorials.add(factorial);
		}
		
		LOGGER.info("Result: {}", sumOfDigits(factorials.get(TARGET).toString()));
	}

	private static int sumOfDigits(String digits)
	{
		int sum = 0;
		
		for (int i = 0; i < digits.length(); i++)
		{
			String digit = digits.substring(i, i+1);
			sum += Integer.parseInt(digit);
		}
		
		return sum;
	}
}
