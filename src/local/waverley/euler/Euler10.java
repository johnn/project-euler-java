/**
 * 
 */
package local.waverley.euler;

import java.util.Iterator;

import local.waverley.maths.prime.Sieve;
import local.waverley.maths.prime.SieveProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler10
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler10.class);
	
	private static long MAX = 2000050;	// Sufficient to get us primes up to just over 2000000.
	private static int TARGET = 2000000;
	
	/**
	 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
	 * 
	 * Find the sum of all the primes below two million.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Sieve sieve = SieveProvider.getSieve(MAX, SieveProvider.SIEVE_OF_ERATOSTHENES);
		LOGGER.info("Prime sieve (using Sieve of Eratosthenes) is {}", sieve);

		Iterator<Long> primes = sieve.getPrimes();
		
		long sum = 0;
		long prime = 0;
		while (primes.hasNext() && prime < TARGET)
		{			
			if (prime > 1)	//TODO Amend SieveProvider so that Sieves don't count 1 as prime...
			{
				sum += prime;
			}
			prime = primes.next();
		}

		LOGGER.info("Last prime was: {}", prime);
		LOGGER.info("Sum of all prime numbers below {} is: {}", TARGET, sum);
	}
}
