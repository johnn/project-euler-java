/**
 * 
 */
package local.waverley.euler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import local.waverley.maths.Perfect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler21
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler21.class);
	
	private static final int TARGET = 10000;
	
	/**
	 * Let d(n) be defined as the sum of proper divisors of n (numbers less than
	 * n which divide evenly into n).
	 * If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair
	 * and each of a and b are called amicable numbers.
	 * 
	 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22,
	 * 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1,
	 * 2, 4, 71 and 142; so d(284) = 220.
	 * 
	 * Evaluate the sum of all the amicable numbers under 10000.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		List<Long> pairs = new ArrayList<Long>();
		Set<Long> amicablePairs = new TreeSet<Long>();
		for (long i = 0; i <= TARGET; i++)
		{
			long sumOfDivisors = Perfect.sumOfProperDivisors(i);
			pairs.add(sumOfDivisors);
			if (i > sumOfDivisors)
			{
				if (pairs.get((int) sumOfDivisors) == i)
				{
					amicablePairs.add(i);
					amicablePairs.add(sumOfDivisors);
				}
			}
		}

		int result = 0;
		for (long pair : amicablePairs)
		{
			result += pair;
		}
		
		LOGGER.info("Result: {}", result);
	}
}
