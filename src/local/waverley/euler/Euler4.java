package local.waverley.euler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Euler4
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler4.class);
	
	/**
	 * A palindromic number reads the same both ways. The largest palindrome
	 * made from the product of two 2-digit numbers is 9009 = 91 99.
	 * Find the largest palindrome made from the product of two 3-digit numbers.
	 * @param args
	 */
	public static void main(String[] args)
	{
		int largest = 0;
		for (int i = 100; i < 1000; i++)
		{
			for (int j = 100; j < 1000; j++)
			{
				int product = i * j;
				if (isPalindrome(product))
				{					
					if (product > largest)
					{
						largest = product;
						LOGGER.info("New largest palindromic product: {} (i: {}, j: {})", new Object[]{product, i, j});
					}
				}
			}
		}
		LOGGER.info("That's all, folks!");
	}
	
	private static boolean isPalindrome(int number)
	{
		String stringNum = String.valueOf(number);
		StringBuffer stringNumRev = new StringBuffer(stringNum);
		stringNumRev.reverse();
		//LOGGER.info("Does {} equal {}?", stringNum, stringNumRev.toString());
		return stringNum.equals(stringNumRev.toString());
	}
}
