/**
 * 
 */
package local.waverley.euler;

import java.math.BigInteger;
import java.util.Iterator;

import local.waverley.maths.Utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler25
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler25.class);
	
	private static final int TARGET = 1000;
	
	/**
	 * The 12th term, F12, is the first term to contain three digits.
	 * 
	 * What is the first term in the Fibonacci sequence to contain 1000 digits?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Iterator<BigInteger> iter = Utility.getFibonacciNumbers();
		BigInteger result = null;
		
		int i = 1;
		while (iter.hasNext())
		{
			BigInteger term = iter.next();
			int numberOfDigits = countDigits(term);
			if (numberOfDigits == TARGET)
			{
				result = term;
				break;
			}
			i++;
		}

		LOGGER.info("Result: {} ({})", i, result.toString());
	}

	private static int countDigits(BigInteger number)
	{
		String numberString = number.toString();
		return numberString.length();
	}
}
