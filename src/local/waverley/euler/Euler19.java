/**
 * 
 */
package local.waverley.euler;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class Euler19
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Euler19.class);
	
	/**
	 * You are given the following information, but you may prefer to do some
	 * research for yourself.
	 * 
	 * 1 Jan 1900 was a Monday.
	 * Thirty days has September,
	 * April, June and November.
	 * All the rest have thirty-one,
	 * Saving February alone,
	 * Which has twenty-eight, rain or shine.
	 * And on leap years, twenty-nine.
	 * A leap year occurs on any year evenly divisible by 4, but not on a
	 * century unless it is divisible by 400.
	 * How many Sundays fell on the first of the month during the twentieth
	 * century (1 Jan 1901 to 31 Dec 2000)?
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		int sundays = 0;
		for (int year = 1901; year <= 2000; year++)
		{
			cal.set(Calendar.YEAR, year);
			for (int month = Calendar.JANUARY; month <= Calendar.DECEMBER; month++)
			{
				cal.set(Calendar.MONTH, month);
				if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
				{
					sundays++;
				}
			}
		}

		LOGGER.info("Result: {}", sundays);
	}
}
