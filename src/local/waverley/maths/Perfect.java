/**
 * 
 */
package local.waverley.maths;

import java.util.Set;

/**
 * @author john
 *
 */
public class Perfect
{
	public static final int DEFICIENT = -1;
	public static final int PERFECT = 0;
	public static final int ABUNDANT = 1;
	
	public static Set<Long> getProperDivisors(long number)
	{
		return Utility.getDivisors(number, true);
	}

	public static int getPerfection(long number)
	{
		long sum = sumOfProperDivisors(number);
		if (sum < number)
		{
			return DEFICIENT;
		}
		else if (sum > number)
		{
			return ABUNDANT;
		}
		else
		{
			return PERFECT;
		}
	}
	
	public static boolean isPerfect(long number)
	{
		return (getPerfection(number) == PERFECT);
	}

	public static long sumOfProperDivisors(long in)
	{
		Set<Long> divisors = getProperDivisors(in);
		return sum(divisors);
	}
	
	private static long sum(Set<Long> divisors)
	{
		long retVal = 0;
		for (long divisor : divisors)
		{
			retVal += divisor;
		}
		return retVal;
	}
}
