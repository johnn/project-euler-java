/**
 * 
 */
package local.waverley.maths.prime;

import java.util.Iterator;

/**
 * @author john
 *
 */
public interface Sieve
{
	public long getSize();
	
	public boolean isPrime(long number) throws SieveException;
	
	public Iterator<Long> getPrimes();
	
	public long getNextPrime(long number) throws SieveException;
}
