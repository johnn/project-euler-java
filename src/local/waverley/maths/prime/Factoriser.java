package local.waverley.maths.prime;

import java.util.Iterator;

public interface Factoriser
{
	Iterator<Long> getFactors(long number, int sieveAlgorithm);
}
