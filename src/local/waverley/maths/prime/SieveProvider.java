/**
 * 
 */
package local.waverley.maths.prime;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 * 
 */
abstract public class SieveProvider
{
	private static final Logger LOGGER = LoggerFactory
									.getLogger(SieveProvider.class);

	private static SieveProvider instance = new SieveProvider() {};

	public static final int SIEVE_TRIAL_DIVISION = 0;
	public static final int SIEVE_OF_ERATOSTHENES = 1;
	public static final int SIEVE_OF_SUNDARAM = 2;

	public static Sieve getSieve(long size, int algorithm)
	{
		switch (algorithm)
		{
			case SIEVE_TRIAL_DIVISION:
				return getSieveByTrialDivision(size);
			case SIEVE_OF_ERATOSTHENES:
				return getSieveByEratosthenesMethod(size);
			case SIEVE_OF_SUNDARAM:
				return getSieveBySundaramMethod(size);
		}
		return null;
	}

	private static Sieve getSieveByTrialDivision(long size)
	{
		//long startTime = System.currentTimeMillis();
		Sieve retVal = instance.new TrialDivisionSieve(size);
		//long endTime = System.currentTimeMillis();
		//LOGGER.debug("TrialDivisionSieve execution time:  {}ms",
		//								(endTime - startTime));
		return retVal;
	}

	private static Sieve getSieveByEratosthenesMethod(long size)
	{
		//long startTime = System.currentTimeMillis();
		Sieve retVal = instance.new SieveOfEratosthenes(size);
		//long endTime = System.currentTimeMillis();
		//LOGGER.debug("SieveOfEratosthenes execution time: {}ms",
		//								(endTime - startTime));
		return retVal;
	}

	private static Sieve getSieveBySundaramMethod(long size)
	{
		//long startTime = System.currentTimeMillis();
		Sieve retVal = instance.new SieveOfSundaram(size);
		//long endTime = System.currentTimeMillis();
		//LOGGER.debug("SieveOfSundaram execution time:     {}ms",
		//								(endTime - startTime));
		return retVal;
	}

	abstract private class AbstractSieve implements Sieve
	{
		protected Hashtable<Long, Boolean> sieve = new Hashtable<Long, Boolean>();
		protected Set<Long> primes = new TreeSet<Long>();

		protected void setPrimes()
		{
			for (long i = 1; i <= sieve.size(); i++)
			{
				if (sieve.get(i))
				{
					primes.add(i);
				}
			}
		}

		public long getSize()
		{
			return sieve.size();
		}

		public boolean isPrime(long number) throws SieveException
		{
			if (number < 1 || number > getSize()) { throw new SieveException(
											"Number out of bounds"); }
			Boolean retVal = sieve.get(number);
			if (retVal == null) { throw new SieveException(
											"Number not in sieve"); }
			return retVal;
		}

		public Iterator<Long> getPrimes()
		{
			return primes.iterator();
		}

		public long getNextPrime(long number) throws SieveException
		{
			Long retVal = null;

			Iterator<Long> iter = primes.iterator();

			while (iter.hasNext())
			{
				long current = iter.next();
				if (current > number)
				{
					retVal = current;
					break;
				}
			}

			if (retVal == null) { throw new SieveException(
											"Next prime not found"); }

			return retVal;
		}

		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			sb.append("{");
			for (int i = sieve.size(); i > 0; i--)
			{
				if (sieve.get(i) && sb.length() < 200)
				{
					sb.append(i);
					if (i > 1)
					{
						sb.append(", ");
					}
				}
			}
			sb.append("}");
			return sb.toString();
		}
	}

	private class TrialDivisionSieve extends AbstractSieve
	{
		public TrialDivisionSieve(long size)
		{
			sieve.put((long) 1, true);
			sieve.put((long) 2, true);

			// Set all even numbers (apart from 2) to false
			for (long i = 4; i <= size; i = i + 2)
			{
				sieve.put(i, false);
			}

			// Check all odd numbers, and set sieve as required
			for (long i = 3; i <= size; i = i + 2)
			{
				sieve.put(i, isPrimeByTrialDivision(i));
			}

			setPrimes();
		}

		private boolean isPrimeByTrialDivision(long value)
		{
			long startNumber = 2;
			double endNumber = Math.sqrt(value);

			for (long i = startNumber; i <= endNumber; i++)
			{
				if ((value % i) == 0) { return false; }
			}

			return true;
		}
	}

	/**
	 * To find all the prime numbers less than or equal to a given integer n by
	 * Eratosthenes' method: Create a list of consecutive integers from 2 to n:
	 * (2, 3, 4, ..., n). Initially, let p equal 2, the first prime number.
	 * Starting from p, count up in increments of p and mark each of these
	 * numbers greater than p itself in the list. These numbers will be 2p, 3p,
	 * 4p, etc.; note that some of them may have already been marked. Find the
	 * first number greater than p in the list that is not marked; let p now
	 * equal this number (which is the next prime). If there were no more
	 * unmarked numbers in the list, stop. Otherwise, repeat from step 3.
	 */
	private class SieveOfEratosthenes extends AbstractSieve
	{
		private long p = 2;

		public SieveOfEratosthenes(long size)
		{
			for (long i = 1; i <= size; i++)
			{
				sieve.put(i, true);
			}

			while (p != 0)
			{
				for (long i = 2 * p; i <= size; i += p)
				{
					sieve.put(i, false);
				}
				p = getNextUnmarkedNumber();
			}

			setPrimes();
		}

		private long getNextUnmarkedNumber()
		{
			long retVal = 0;

			for (long i = p + 1; i <= sieve.size(); i++)
			{
				if (sieve.get(i))
				{
					retVal = i;
					break;
				}
			}

			return retVal;
		}
	}

	private class SieveOfSundaram extends AbstractSieve
	{
		public SieveOfSundaram(long size)
		{
			Set<Long> set = new TreeSet<Long>();
			for (long i = 1; i <= size; i++)
			{
				set.add(i);
			}

			// Outer:
			for (long i = 1; i <= size; i++)
			{
				// LOGGER.debug("SieveOfSundaram: i: {}", i);
				for (long j = i; j <= size; j++)
				{
					// LOGGER.debug("SieveOfSundaram: j: {}", j);

					long number = i + j + (2 * i * j);
					if (number > size)
					{
						// break Outer;
					}
					else
					{
						set.remove(number);
					}
				}
			}

			sieve.put((long) 1, true);
			sieve.put((long) 2, true);
			for (long i = 3; i <= size; i++)
			{
				sieve.put(i, false);
			}

			Iterator<Long> itor = set.iterator();
			long number = 0;
			while (itor.hasNext() && number <= size)
			{
				number = itor.next();
				number = (number * 2) + 1;
				if (number <= size) // Nasty hackery, hint at where to optimise.
				{
					sieve.put(number, true);
				}
			}

			setPrimes();
		}
	}
}
