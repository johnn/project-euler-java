/**
 * 
 */
package local.waverley.maths.prime;

/**
 * @author john
 *
 */
public class SieveException extends Exception
{
	private static final long serialVersionUID = 1L;

	public SieveException(String string)
	{
		super(string);
	}

	
}
