/**
 * 
 */
package local.waverley.maths.prime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class FactoriserProvider
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FactoriserProvider.class);
	
	private static FactoriserProvider instance = new FactoriserProvider() {};
	
	public static Factoriser getFactoriserByTrialDivision()
	{
		long startTime = System.currentTimeMillis();
		Factoriser retVal = instance.new TrialDivisionFactoriser();
		long endTime = System.currentTimeMillis();
		LOGGER.debug("getFactoriserByTrialDivision execution time:  {}ms", (endTime - startTime));
		return retVal;
	}

	private class TrialDivisionFactoriser implements Factoriser
	{
		public Iterator<Long> getFactors(long number, int sieveAlgorithm)
		{
			List<Long> primeFactors = new ArrayList<Long>();
			
			if (number == 1)
			{
				primeFactors.add((long) 1);
				return primeFactors.listIterator();
			}
			
			long size = (long) (Math.sqrt(number) + 1);			
			Sieve sieve = SieveProvider.getSieve(size, sieveAlgorithm);

			long startTime = System.currentTimeMillis();
			
			Iterator<Long> primes = sieve.getPrimes();
			
			long prime = 0;
			while (primes.hasNext() && !((prime * prime) > number))
			{
				prime = primes.next();
				if (prime == 1) { prime = primes.next(); }
				
				//LOGGER.debug("Current prime: {}", prime);
				
				if ((prime * prime) > number)
				{
					//LOGGER.debug("(prime * prime) > number");
					break;
				}
				
				while ((number % prime) == 0)
				{
					primeFactors.add((long) prime);					
					number = (long) (number / prime);
					
					LOGGER.debug("Added prime factor: {}, number is now: {}", prime, number);
				}
			}
			
			if (number > 1)
			{
				primeFactors.add(number);
				LOGGER.debug("Added prime factor: {}", number);
			}
			
			long endTime = System.currentTimeMillis();
			LOGGER.debug("getFactors execution time:  {}ms (start {}, end {})",
											new Object[]{(endTime - startTime), startTime, endTime});
			
			return primeFactors.listIterator();
		}
	}
}
