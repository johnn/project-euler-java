/**
 * 
 */
package local.waverley.maths;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author john
 *
 */
public class Utility
{
	public static Set<Long> getDivisors(long number)
	{
		return getDivisors(number, false);
	}
	
	/**
	 * @deprecated Use {@link Perfect#getProperDivisors(long)} instead
	 */
	public static Set<Long> getProperDivisors(long number)
	{
		return Perfect.getProperDivisors(number);
	}
	
	public static Set<Long> getDivisors(long number, boolean proper)
	{
		Set<Long> retVal = new TreeSet<Long>();
		
		long testDivisor = 2;
		long currentEndPoint = number / testDivisor;
		
		while (testDivisor <= currentEndPoint)
		{
			if ((number % testDivisor) == 0)
			{
				retVal.add(testDivisor);
				retVal.add(number / testDivisor);
				currentEndPoint = number / testDivisor;
			}
			testDivisor++;
		}
		
		retVal.add(1L);
		if (!proper)
		{
			retVal.add(number);
		}
		
		return retVal;
	}
	
	
	
	public static Iterator<Long> getTriangleNumbers()
	{
		return new Iterator<Long>() {

			private long index = 1;
			
			@Override
			public boolean hasNext()
			{
				return true;
			}

			@Override
			public Long next()
			{
				long retVal = 0;
				for (long i = 1; i <= index; i++)
				{
					retVal += i;
				}
				index++;
				return retVal;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();
			}			
		};
	}
	
	public static Iterator<Long> getHailstoneSequence(final long start)
	{
		return new Iterator<Long>() {

			long currentTerm = start;
			long nextTerm = start;
			
			@Override
			public boolean hasNext()
			{
				return currentTerm != 1;
			}

			@Override
			public Long next()
			{
				currentTerm = nextTerm;
				
				if ((currentTerm % 2) == 0)
				{
					nextTerm = currentTerm / 2;
				}
				else
				{
					nextTerm = (3 * currentTerm) + 1;
				}
				
				return currentTerm;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();				
			}			
		};
	}
	
	public static Iterator<List<Long>> getPascalsTriangle()
	{
		return new Iterator<List<Long>>() {

			List<Long> currentRow = getFirstRow();
			List<Long> nextRow = currentRow;
			
			@Override
			public boolean hasNext()
			{
				return true;
			}

			@Override
			public List<Long> next()
			{
				currentRow = nextRow;
				
				nextRow = new ArrayList<Long>();
				long leftItem = 0;
				
				for (long rightItem : currentRow)
				{
					long sum = leftItem + rightItem;
					nextRow.add(sum);
					leftItem = rightItem;
				}
				nextRow.add(1L);

				return currentRow;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();				
			}
			
			private List<Long> getFirstRow()
			{
				List<Long> retVal = new ArrayList<Long>();
				retVal.add(1L);
				return retVal;
			}
		};		
	}

	public static <T> Set<T> copy(Set<T> set)
	{
		Set<T> retVal = new TreeSet<T>();
		for (T item : set)
		{
			retVal.add(item);
		}		
		return retVal;
	}
	
	public static Iterator<BigInteger> getFibonacciNumbers()
	{
		return new Iterator<BigInteger>() {
			
			private BigInteger[] terms = { BigInteger.ZERO, BigInteger.ONE };

			@Override
			public boolean hasNext()
			{
				return true;
			}

			@Override
			public BigInteger next()
			{
				BigInteger retVal = terms[1]; 
				
				BigInteger nextTerm = terms[0].add(terms[1]);
				terms[0] = terms[1];
				terms[1] = nextTerm;
				
				return retVal;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();				
			}
			
		};
	}
}
