package local.waverley.utility;

public interface Nullable
{
	public boolean isNull();
}