package local.waverley.utility;

import java.util.Set;

public class Debug
{
	//private static final Logger LOGGER = LoggerFactory.getLogger(Debug.class);
									
	@SuppressWarnings("unused")
	public static <T> String debugSet(Set<T> set)
	{
		StringBuffer sb = new StringBuffer();
		for (T item : set)
		{
			if (sb.length() > 0)
			{
				sb.append(", ");
			}
			sb.append(item);
		}
		return sb.toString();
	}
}
