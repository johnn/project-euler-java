/**
 * 
 */
package local.waverley.maths;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author john
 *
 */
public class UtilityTest
{
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}

	/**
	 * Test method for {@link local.waverley.maths.Utility#getDivisors(long)}.
	 */
	@Test
	public final void testGetDivisors()
	{
		long testNumber = 1;
		Set<Long> testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 1);
		assertTrue(testSet.contains(1L));
		
		testNumber = 2;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 2);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		
		testNumber = 3;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 2);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(3L));
		
		testNumber = 4;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 3);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		assertTrue(testSet.contains(4L));

		testNumber = 5;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 2);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(5L));

		testNumber = 6;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 4);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		assertTrue(testSet.contains(3L));
		assertTrue(testSet.contains(6L));
		
		testNumber = 28;
		testSet = Utility.getDivisors(testNumber);
		assertTrue(testSet.size() == 6);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		assertTrue(testSet.contains(4L));
		assertTrue(testSet.contains(7L));
		assertTrue(testSet.contains(14L));
		assertTrue(testSet.contains(28L));
	}

	/**
	 * Test method for {@link local.waverley.maths.Utility#getTriangleNumbers()}.
	 */
	@Test
	public final void testGetTriangleNumbers()
	{
		Iterator<Long> triangles = Utility.getTriangleNumbers();
		assertTrue(triangles.hasNext());
		assertTrue(triangles.next() == 1);
		assertTrue(triangles.next() == 3);
		assertTrue(triangles.next() == 6);
		assertTrue(triangles.next() == 10);
		assertTrue(triangles.next() == 15);
		assertTrue(triangles.next() == 21);
		assertTrue(triangles.next() == 28);
		assertTrue(triangles.hasNext());
	}

	/**
	 * Test method for {@link local.waverley.maths.Utility#getHailstoneSequence(long)}.
	 */
	@Test
	public final void testGetHailstoneSequence()
	{
		Iterator<Long> hailstones = Utility.getHailstoneSequence(13);
		// 13  40  20  10  5  16  8  4  2  1
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 13);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 40);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 20);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 10);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 5);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 16);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 8);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 4);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 2);
		assertTrue(hailstones.hasNext());
		assertTrue(hailstones.next() == 1);
		assertFalse(hailstones.hasNext());
	}
	
	/**
	 * Test method for {@link local.waverley.maths.Utility#getPascalsTriangle()}.
	 */
	@Test
	public final void testGetPascalsTriangle()
	{
		//final Logger LOGGER = LoggerFactory.getLogger(UtilityTest.class);
		
		Iterator<List<Long>> triangle = Utility.getPascalsTriangle();
		
		assertTrue(triangle.hasNext());		
		Iterator<Long> row = triangle.next().iterator();
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertFalse(row.hasNext());

		assertTrue(triangle.hasNext());
		row = triangle.next().iterator();
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertFalse(row.hasNext());
		
		assertTrue(triangle.hasNext());
		row = triangle.next().iterator();
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 2);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertFalse(row.hasNext());
		
		assertTrue(triangle.hasNext());
		row = triangle.next().iterator();
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 3);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 3);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertFalse(row.hasNext());
		
		assertTrue(triangle.hasNext());
		row = triangle.next().iterator();
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 4);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 6);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 4);
		assertTrue(row.hasNext());
		assertTrue(row.next() == 1);
		assertFalse(row.hasNext());
	}
	
	/**
	 * Test method for {@link local.waverley.maths.Utility#copy(Set<T>)}.
	 */
	@Test
	public final void testCopy()
	{
		Set<Integer> setInts = new TreeSet<Integer>();
		setInts.add(1);
		setInts.add(2);
		setInts.add(3);
		Set<Integer> testSetInts = Utility.copy(setInts);
		assertTrue(testSetInts.size() == setInts.size());
		
		Set<String> setStrings = new TreeSet<String>();
		setStrings.add("A");
		setStrings.add("B");
		setStrings.add("C");
		Set<String> testSetStrings = Utility.copy(setStrings);
		assertTrue(testSetStrings.size() == setStrings.size());
	}
	
	/**
	 * Test method for {@link local.waverley.maths.Utility#getFibonacciNumbers(Iterator<BigInteger>)}.
	 */
	@Test
	public final void testGetFibonacciNumbers()
	{
		Iterator<BigInteger> iter = Utility.getFibonacciNumbers();
		
		assertTrue(iter.hasNext());
		BigInteger next1 = iter.next();
		assertTrue(next1.compareTo(new BigInteger("1")) == 0);
		
		assertTrue(iter.hasNext());
		BigInteger next2 = iter.next();
		assertTrue(next2.compareTo(new BigInteger("1")) == 0);
		
		assertTrue(iter.hasNext());
		BigInteger next3 = iter.next();
		assertTrue(next3.compareTo(new BigInteger("2")) == 0);
		
		assertTrue(iter.hasNext());
		BigInteger next4 = iter.next();
		assertTrue(next4.compareTo(new BigInteger("3")) == 0);
		
		assertTrue(iter.hasNext());
		BigInteger next5 = iter.next();
		assertTrue(next5.compareTo(new BigInteger("5")) == 0);
		
		assertTrue(iter.hasNext());
		BigInteger next6 = iter.next();
		assertTrue(next6.compareTo(new BigInteger("8")) == 0);
	}
}
