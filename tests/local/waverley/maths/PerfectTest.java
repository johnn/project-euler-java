/**
 * 
 */
package local.waverley.maths;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author john
 *
 */
public class PerfectTest
{

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}

	/**
	 * Test method for {@link local.waverley.maths.Perfect#getProperDivisors(long)}.
	 */
	@Test
	public final void testGetProperDivisors()
	{
		long testNumber = 1;
		Set<Long> testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 1);
		assertTrue(testSet.contains(1L));
		
		testNumber = 2;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 1);
		assertTrue(testSet.contains(1L));
		
		testNumber = 3;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 1);
		assertTrue(testSet.contains(1L));
		
		testNumber = 4;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 2);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));

		testNumber = 5;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 1);
		assertTrue(testSet.contains(1L));

		testNumber = 6;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 3);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		assertTrue(testSet.contains(3L));
		
		testNumber = 28;
		testSet = Perfect.getProperDivisors(testNumber);
		assertTrue(testSet.size() == 5);
		assertTrue(testSet.contains(1L));
		assertTrue(testSet.contains(2L));
		assertTrue(testSet.contains(4L));
		assertTrue(testSet.contains(7L));
		assertTrue(testSet.contains(14L));
	}

	/**
	 * Test method for {@link local.waverley.maths.Perfect#getPerfection(long)}.
	 */
	@Test
	public final void testGetPerfection()
	{
		long testNumber = 1;
		assertTrue(Perfect.getPerfection(testNumber) == Perfect.PERFECT);
		testNumber = 2;
		assertTrue(Perfect.getPerfection(testNumber) == Perfect.DEFICIENT);
		testNumber = 12;
		assertTrue(Perfect.getPerfection(testNumber) == Perfect.ABUNDANT);
	}

	/**
	 * Test method for {@link local.waverley.maths.Perfect#isPerfect(long)}.
	 */
	@Test
	public final void testIsPerfect()
	{
		long testNumber = 1;
		assertTrue(Perfect.isPerfect(testNumber));
		testNumber = 28;
		assertTrue(Perfect.isPerfect(testNumber));
	}

	/**
	 * Test method for {@link local.waverley.maths.Perfect#sumOfProperDivisors(long)}.
	 */
	@Test
	public final void testSumOfProperDivisors()
	{
		long testNumber = 1;
		assertTrue(Perfect.sumOfProperDivisors(testNumber) == 1);
		testNumber = 2;
		assertTrue(Perfect.sumOfProperDivisors(testNumber) == 1);
		testNumber = 3;
		assertTrue(Perfect.sumOfProperDivisors(testNumber) == 1);
		testNumber = 4;
		assertTrue(Perfect.sumOfProperDivisors(testNumber) == 3);
	}
}
