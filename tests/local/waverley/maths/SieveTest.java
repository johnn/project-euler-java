/**
 * 
 */
package local.waverley.maths;

import static org.junit.Assert.*;

import java.util.Iterator;

import local.waverley.maths.prime.Sieve;
import local.waverley.maths.prime.SieveException;
import local.waverley.maths.prime.SieveProvider;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author john
 *
 */
public class SieveTest
{
	private Sieve trialDivisionSieve = null;
	private Sieve sieveOfEratosthenes = null;
	private Sieve sieveOfSundaram = null;
	
	private static final int MAX_NUM = 50;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		trialDivisionSieve = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_TRIAL_DIVISION);
		sieveOfEratosthenes = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_OF_ERATOSTHENES);
		sieveOfSundaram = SieveProvider.getSieve(MAX_NUM, SieveProvider.SIEVE_OF_SUNDARAM);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}

	/**
	 * Test method for {@link local.waverley.maths.prime.Sieve#getSize()}.
	 */
	@Test
	public final void testGetSize()
	{
		assertTrue(trialDivisionSieve.getSize() == MAX_NUM);
		assertTrue(sieveOfEratosthenes.getSize() == MAX_NUM);
		assertTrue(sieveOfSundaram.getSize() == MAX_NUM);
	}

	/**
	 * Test method for {@link local.waverley.maths.prime.Sieve#isPrime(int)}.
	 */
	@Test
	public final void testIsPrime()
	{
		try
		{
			assertTrue(trialDivisionSieve.isPrime(1));
			assertTrue(sieveOfEratosthenes.isPrime(1));
			assertTrue(sieveOfSundaram.isPrime(1));
			assertTrue(trialDivisionSieve.isPrime(2));
			assertTrue(sieveOfEratosthenes.isPrime(2));
			assertTrue(sieveOfSundaram.isPrime(2));
			assertTrue(trialDivisionSieve.isPrime(3));
			assertTrue(sieveOfEratosthenes.isPrime(3));
			assertTrue(sieveOfSundaram.isPrime(3));
			assertTrue(trialDivisionSieve.isPrime(43));
			assertTrue(sieveOfEratosthenes.isPrime(43));
			assertTrue(sieveOfSundaram.isPrime(43));
			assertTrue(trialDivisionSieve.isPrime(47));
			assertTrue(sieveOfEratosthenes.isPrime(47));
			assertTrue(sieveOfSundaram.isPrime(47));			
			
			assertFalse(trialDivisionSieve.isPrime(4));
			assertFalse(sieveOfEratosthenes.isPrime(4));
			assertFalse(sieveOfSundaram.isPrime(4));
			assertFalse(trialDivisionSieve.isPrime(6));
			assertFalse(sieveOfEratosthenes.isPrime(6));
			assertFalse(sieveOfSundaram.isPrime(6));
			assertFalse(trialDivisionSieve.isPrime(46));
			assertFalse(sieveOfEratosthenes.isPrime(46));
			assertFalse(sieveOfSundaram.isPrime(46));
			assertFalse(trialDivisionSieve.isPrime(48));
			assertFalse(sieveOfEratosthenes.isPrime(48));
			assertFalse(sieveOfSundaram.isPrime(48));
			assertFalse(trialDivisionSieve.isPrime(49));
			assertFalse(sieveOfEratosthenes.isPrime(49));
			assertFalse(sieveOfSundaram.isPrime(49));
			assertFalse(trialDivisionSieve.isPrime(50));
			assertFalse(sieveOfEratosthenes.isPrime(50));
			assertFalse(sieveOfSundaram.isPrime(50));
		}
		catch (SieveException e)
		{
			fail(e.getLocalizedMessage());
		}
	}

	/**
	 * Test method for {@link local.waverley.maths.prime.Sieve#getPrimes()}.
	 */
	@Test
	public final void testGetPrimes()
	{
		Iterator<Long> trialDivisionPrimes = trialDivisionSieve.getPrimes();
		Iterator<Long> sieveOfEratosthenesPrimes = sieveOfEratosthenes.getPrimes();
		Iterator<Long> sieveOfSundaramPrimes = sieveOfSundaram.getPrimes();
		
		if (!trialDivisionPrimes.hasNext())
		{
			fail("Empty iterator!");
		}
		
		if (!sieveOfEratosthenesPrimes.hasNext())
		{
			fail("Empty iterator!");
		}
		
		if (!sieveOfSundaramPrimes.hasNext())
		{
			fail("Empty iterator!");
		}
	}

	/**
	 * Test method for {@link local.waverley.maths.prime.Sieve#getNextPrime(int)}.
	 */
	@Test
	public final void testGetNextPrime()
	{
		try
		{
			assertEquals(trialDivisionSieve.getNextPrime(37), 41);
			assertEquals(sieveOfEratosthenes.getNextPrime(37), 41);
			assertEquals(sieveOfSundaram.getNextPrime(37), 41);
			assertEquals(trialDivisionSieve.getNextPrime(38), 41);
			assertEquals(sieveOfEratosthenes.getNextPrime(38), 41);
			assertEquals(sieveOfSundaram.getNextPrime(38), 41);
		}
		catch (SieveException e)
		{
			fail(e.getLocalizedMessage());
		}
	}

}
