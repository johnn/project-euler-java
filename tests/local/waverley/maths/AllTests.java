package local.waverley.maths;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PerfectTest.class, SieveTest.class, UtilityTest.class })
public class AllTests
{

}
